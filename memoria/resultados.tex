
\chapter{Resultados}
\label{cha:resultados}

En este capítulo se describirán las diferentes aplicaciones del modelo de
comunicaciones desarrollado a diferentes situaciones en el ámbito del proyecto
\emph{Elcano}. Igualmente, se hace un análisis del esfuerzo de desarrollo del
proyecto y del tiempo dedicado al mismo, así como una estimación del coste
total que ha supuesto su realización.

\section{Aplicaciones del modelo a diferentes escenarios}
\label{sec:aplic-icedds}
En esta sección se enumeran diferentes escenarios en un sistema de localización
de usuarios en el interior de edificios para justificar las diferentes formas
de filtrado que se han elaborado en este proyecto.

Los eventos de localización que envía el sistema \emph{Elcano} son del tipo que
se muestra en el listado \ref{code:posicionelcano}. Los eventos de
posicionamiento contienen la variable \emph{msid} que es el identificador de un
determinado proveedor de eventos, \emph{time} es el momento del envío del
evento y \emph{theShape} contiene el tipo de datos que sirve para posicionar al
usuario en el entorno. El \acs{Slice} que contiene todos los diferentes tipos
que pueden ir en el evento se pueden ver en el apéndice \ref{chap:slices}.

\begin{listing}[
  language = python,
  caption  = {Evento de localización de \emph{Elcano}},
  label    = code:posicionelcano]
  struct Position {
    string msid;
    long time;
    Shape theShape;
  };
\end{listing}

Para simplificar los escenarios que se van a explicar a continuación, se supone
que los eventos enviados son del tipo \emph{MLP.Coord}. Las coordenadas
\emph{x},\emph{y} y \emph{z} representan los ejes del plano, donde la variable
\emph{x} representa al eje horizontal, la variable \emph{y} representa el eje
vertical y la variable \emph{z} correspondería a la altura. Esta última
variable no se tiene en cuenta debido a que la representación de los eventos de
localización se realizan sobre un plano en 2 dimensiones.

La forma de expresar los filtros, al ser una cadena o un conjunto de cadenas,
proporciona mayores posibilidades para indicar un conjunto de filtros que sean
los más apropiados para las necesidades de cada suscripción. En cada expresión
de filtro, además de poder indicar los filtros descritos en la tabla
\ref{tab:operadorescom}, se pueden añadir los operadores lógicos de la tabla
\ref{tab:operadores} para obtener filtros mucho más versátiles. De esta
manera, el conjunto de filtros puede estar determinado por una especificación
mucho más detallada que se ajustaría más a las exigencias de cada suscriptor.
\begin{table}[h!]
  \centering
  \input{tables/operadorescomparativos.tex}
  \caption{Operadores comparativos para expresar filtros}
  \label{tab:operadorescom}
\end{table}

\begin{table}[h!]
  \centering
  \input{tables/operadoreslogicos.tex}
  \caption{Operadores lógicos para filtros}
  \label{tab:operadores}
\end{table}

\subsection{Escenario 1: Canales filtrados}
% Canales filtrados
Pueden existir canales que envíen eventos en determinadas áreas. El mapa se
divide por zonas y se crea un canal de eventos en cada una de estas zonas. En
este caso específico, se va a suponer que hay tres canales de eventos cuyo
filtro será la especificación de un área determinada. En la figura
\ref{fig:mapa3canales} se muestran las áreas correspondientes a cada canal.

\begin{figure}[h]
  \begin{center}
    \includegraphics[width=0.6\textwidth]{filtrodecanales.png}
    \caption{Vista de las áreas que cubre cada canal en el mapa}
    \label{fig:mapa3canales}
  \end{center}
\end{figure}

Los tres canales filtrarán en la variable ``x''. El canal 1 manejará los
eventos con valores de ``x'' con rango entre 15 y 34, el canal 2 los valores
con rango entre 34 y 52 y el canal 3 los valore con rango entre 52 y 70. De
esta manera se tienen cubiertas todas las zonas donde el usuario puede navegar.

Un usuario estará suscrito a esos canales y recibirá sólo los eventos desde los
canales acorde al área en la que se sitúe.

Para ver el efecto de este filtrado en los diferentes canales, se simula la
ruta de la figura \ref{fig:rutaescenario1}, donde el usuario se traslada desde
la posición (20, 24) hasta la posición (63, 32).

\begin{figure}[h]
  \begin{center}
    \includegraphics[width=0.6\textwidth]{rutaescenario1.png}
    \caption{Ruta del usuario en el escenario 1}
    \label{fig:rutaescenario1}
  \end{center}
\end{figure}

Si se simula esta situación en la aplicación \emph{IceDDSAndroid}, se pueden
observar que el punto mediante el cual se representa al usuario cambia de color
al pasar de una zona a otra. Previamente, el usuario ha
tenido que suscribirse a los canales, ya sea de manera manual o porque la
aplicación subscribe a los usuarios del sistema directamente. En la figura
\ref{fig:rastrousuarioruta1} se puede ver el rastro del camino del usuario. En
la aplicación se muestra el punto únicamente donde se encuentra el usuario en
cada momento, de manera intermitente, pero en la figura
\ref{fig:rastrousuarioruta1} se muestra una vista del camino completo para su
mejor entendimiento.

\begin{figure}[h]
  \begin{center}
    \includegraphics[width=0.7\textwidth]{rastrousuarioruta1.png}
    \caption{Ruta del usuario en la aplicación \emph{IceDDSAndroid}}
    \label{fig:rastrousuarioruta1}
  \end{center}
\end{figure}


\subsection{Escenario 2: Subscriptores filtrados}

En la aplicación \emph{Elcano} pueden recibirse multitud de eventos de
localización diferentes que proceden de diferentes proveedores de eventos
(\ac{WiFi}, Bluetooh y \ac{RFID}). Además de los eventos de localización se
recibe información del entorno, tales como tareas, rutas, etc.

Si un usuario está viendo en la pantalla de la tablet cierta parte del mapa,
lo más lógico es pensar que sólo le interesarán los eventos que se reciban en
ese área específica (figura \ref{fig:escenariosubconfiltros}). El usuario, por
lo tanto, estará suscrito a los canales de eventos indicando sus propios
filtros que serán los que delimiten el área mostrada en pantalla. Cuando hay un
desplazamiento de la pantalla, bien porque se ha generado un deslizamiento con
el dedo, o se ha modificado el zoom, este filtro cambiará para indicar el nuevo
área de visualización.

\begin{figure}[h]
  \begin{center}
    \includegraphics[width=0.7\textwidth]{escenariosubconfiltros.png}
    \caption{Visión que muestra la pantalla del dispositivo con respecto al
      mapa completo}
    \label{fig:escenariosubconfiltros}
  \end{center}
\end{figure}

La posibilidad de cambiar el filtro de la suscripción disminuye la creación de
canales dentro del sistema, ya que la suscripción con filtros supone la
creación de un canal filtrado dentro de sistema (capítulo
\ref{sec:filtrado-suscripcion}), pero de esta manera no hace falta una nueva
suscripción, simplemente se cambian los filtros del canal deseado.

\subsection{Escenario 3: Publicadores filtrados}

En capítulos anteriores se ha descrito como los canales pueden tener varios
publicadores y además restringir los datos que envían los mismos indicando un
conjunto de filtros.

Con esta perspectiva, la situación que se trata en este escenario es la
existencia de varios publicadores de un canal global que se encargan de enviar
eventos atendiendo a coordenadas específicas. En la figura
\ref{fig:escenariopubconfiltros} se pueden observar los distintos publicadores
que podrían existir en un canal que maneja eventos correspondientes al mapa
completo donde puede navegar el usuario.

\begin{figure}[h]
  \begin{center}
    \includegraphics[width=0.7\textwidth]{escenariopubconfiltros.png}
    \caption{Publicadores existentes que limitan el envío de datos a
      zonas concretas}
    \label{fig:escenariopubconfiltros}
  \end{center}
\end{figure}

Cada publicador se encarga de enviar los eventos de localización
correspondientes a una zona específica y así, la responsabilidad del envío de
datos estaría repartida entre varios. En cada publicador se puede limitar los
eventos de localización que se salen del mapa, las zonas por las que el usuario
no tiene acceso, otras zonas donde no es relevante mantener la información de la
posición del usuario (por ej.: los aseos), \...

\subsection{Escenario 4: Federación de canales filtrados}

Los servicios que ofrece el proyecto \emph{Elcano} se podrían ampliar y
expandir a varios edificios de la Escuela de Informática. De este modo, se
tendría un control de los usuarios en los diferentes edificios. Cada edificio
supone la recogida de información de su entorno, así como definir las tareas
que los usuarios pueden realizar y los caminos o rutas que deben seguir desde su
posición a la tarea elegida.

\begin{figure}[h]
  \begin{center}
    \includegraphics[width=0.7\textwidth]{escenariofederacion.png}
    \caption{Federación de canales filtrados}
    \label{fig:escenariofederacion}
  \end{center}
\end{figure}

El escenario descrito constaría de un monitor de sistema donde se mostraría
los eventos de localización de todos los edificios. Estos eventos se enviarían
a un canal que a su vez propagaría la información a los diferentes canales que
representarían a cada edificio. En la figura \ref{fig:escenariofederacion} se
muestra un esquema general del funcionamiento del sistema.

En este supuesto, se puede ver claramente que los datos serán propagados por
medio de los enlaces realizados y por lo tanto, cada canal de eventos de cada
edificio podrá a su vez tener sus propios suscriptores (filtrados y no
filtrados).


\section{Recursos y costes}
\label{sec:recursos-y-costes}

La duración del proyecto ha sido de 10 meses, aunque el tiempo dedicado al
desarrollo del proyecto ha estado compaginado con el trabajo dentro del grupo
\acs{Arco}, con lo cual no se ha podido realizar una dedicación a tiempo
completo.

Al inicio, fue necesario el estudio y el enriquecimiento sobre el estándar
\acs{DDS} en el que se centra el proyecto, así como aprender a utilizar
las diferentes tecnologías que se han empleado para su desarrollo (Icestorm,
Android, etc.). Todo esto supuso un incremento del esfuerzo realizado para la
elaboración de este proyecto.

A continuación, se muestra el número de líneas de código del proyecto divididas
por los diferentes elementos desarrollados (tabla
\ref{tab:lineasporelemento}).

La tabla \ref{tab:lineasporlenguaje} muestra un resumen del número líneas de
código totales por cada uno de los lenguajes de programación empleados.


Finalmente, la tabla \ref{tab:estimacioncostes} presenta la estimación del
coste del proyecto generado usando \texttt{sloccount}~\cite{sloccount} basado
en el modelo COCOMO. En esta estimación no se tiene en cuenta los \acs{IDL} y
\acs{Slice} definidos, ya que la herramienta no proporciona soporte para los
mismos.

\subsection{Repositorio}
\label{sec:repositorio}

El código del proyecto se encuentra en \emph{bitbucket.org}~\cite{bitbucket}, que es
un servicio de alojamiento basado en web para proyectos. Para descargar el
repositorio se ejecuta la siguiente sentencia en un terminal:
\begin{listing}[
  language = python]
$ hg clone https://bitbucket.org/arco_group/icedds
\end{listing} %$

Este servicio permite utilizar \emph{Mercurial}~\cite{mercurial} como sistema
de control de versiones. Esta herramienta nos permite ver los cambios
realizados en cada actualización, lo que permite hacer un seguimiento del
desarrollo del proyecto.

El repositorio de la documentación también se encuentra en \emph{bitbucket.org}
y se descarga con la siguiente sentencia:

\begin{listing}[
  language = python]
$ hg clone https://bitbucket.org/arco_group/pfc.icedds
\end{listing} %$


\begin{table}[hb]
  \centering
  \input{tables/codigoporelemento.tex}
  \caption{Líneas de código separado por elementos}
  \label{tab:lineasporelemento}
\end{table}

\begin{table}[hb]
  \centering
  \input{tables/resumenporlenguaje.tex}
  \caption{Líneas de código por lenguaje de programación}
  \label{tab:lineasporlenguaje}
\end{table}

\begin{table}[hb]
  \centering
  \input{tables/estimaciondelcoste.tex}
  \caption{Estimación de costes del proyecto}
  \label{tab:estimacioncostes}
\end{table}
