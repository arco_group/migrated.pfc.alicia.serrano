
\chapter{Casos de estudio: OpenSplice y RTI}
\label{cha:casos-de-estudio}

\section{RTI Context DDS}
\label{sec:rti-context-dds}

\emph{RTI Data Distribution Service} (RTI DDS) es un middleware de red para
aplicaciones distribuidas en tiempo real. \emph{RTI DDS}
simplifica el desarrollo, implementación y mantenimiento de aplicaciones y
proporciona una distribución rápida y predecible de los datos en un conjunto de
redes de transporte.

La clave del éxito de este modelo es que las aplicaciones que utilizan este
modelo están totalmente desacopladas. Se emplea muy poco tiempo en diseñar la
forma en las que estas aplicaciones interactúan.

\emph{RTI DDS} se encarga de automatizar todos los aspectos de la entrega de
mensajes, sin necesidad de ninguna intervención por parte de las aplicaciones
de usuario, incluyendo:
\begin{itemize}
\item Determina quién debe recibir los mensajes.
\item Dónde se encuentran los destinatarios de los mensajes.
\item Qué ocurre si los mensajes no pueden ser entregados.
\end{itemize}

\emph{RTI DDS} incluye las siguientes características, que han sido
diseñadas para satisfacer las necesidades de las aplicaciones de tiempo real
distribuidas:

\begin{description}
\item[Centro de datos para las comunicaciones publicador/suscriptor] Simplifica
  la programación de las aplicaciones y garantiza que los datos lleguen a su
  destino con la mínima latencia.
\item[Tipos de datos definidos por el usuario] Habilita a los usuarios para
  especificar el formato de la información que será enviada a cada aplicación.
\item[Mensajes fiables] Las aplicaciones que se dedican a la suscripción pueden
  especificar la entrega fiable de muestras.
\item[Múltiples dominios de comunicaciones] Pueden haber múltiples dominios
  utilizando la misma red física y cada aplicación puede ser configurada para
  participar en múltiples dominios.
\item[Arquitectura simétrica] No hay servidor centralizado ni nodos
  privilegiados. Las suscripciones y las publicaciones puede ser añadidas
  y eliminadas dinámicamente del sistema en cualquier momento.
\item[Frameworks de transporte] \emph{RTI DDS} incluye la habilidad para
  definir nuevas herramientas de transporte. \emph{RTI DDS} especifica un
  \emph{plugin} de transporte UDP/IP y transporte de memoria compartida. Puede
  ser configurado para trabajar sobre gran variedad de mecanismos, tecnologías
  de redes, etc.
\item[Soporte multilenguaje] Soporta \acs{API}s para los lenguajes C, C++,
  C++/CLI, C\# y Java.
\item[Soporte multiplataforma] Incluye soporte para UNIX, sistemas de
  tiempo-real (INTEGRITY, VxWorks, QNX y LynxOS) y Windows (2000, 2003, CE,
  Vista y XP).
\item[Cumplimiento de estándar] La \acs{API} cumple la capa \acs{DCPS} de la
  especificación de \acs{DDS} de la \acs{OMG}. El formado de los paquetes de
  datos que envía \emph{RTI DDS} cumple con la especificación del protocolo
  \acs{RTPS}.
\end{description}

Un ejemplo básico para ver el funcionamiento de un publicador y un suscriptor
utilizando \emph{RTI DDS} se puede encontrar en el cdrom adjunto en el
directorio \texttt{src/samples/RTI/HelloWorld} y además la prueba de
integración correspondiente en el directorio \texttt{test/samples}. Este
ejemplo no se analiza ya que los ejemplos realizados con \emph{OpenSplice}
especificados en el próximo capítulo son más claros para conocer la dinámica de
\acs{DDS}.

\section{OpenSplice DDS}
\label{sec:opensplice}

\subsection{Arquitecture OpenSplice DDS}
\label{sec:arqu-openspl-dds}

\emph{OpenSplice DDS} compone una arquitectura que utiliza memoria compartida
para conectar no sólo todas las aplicaciones que residen dentro de un nodo
computacional, sino también el conjunto de servicios que ofrecen diferentes
\emph{hosts}\footnote{Nodo conectada a la red que proporciona servicios y
  solicita servicios de otros.}. Esta arquitectura garantiza la escalabilidad,
flexibilidad y extensibilidad.

En la memoria compartida que utiliza esta arquitectura, los datos están
físicamente una vez en cada máquina y la administración proporciona a cada
suscriptor una vista privada de esos datos. Esto permite que una caché de datos
del suscriptor será percibida como una base de datos individual que puede estar
filtrada, encolada, etc.

\emph{OpenSplice DDS} puede ser fácilmente configurable especificando los
servicios necesarios que se utilizarán, así como la configuración de los
servicios que persigue un acoplamiento óptimo en el dominio de aplicación (los
parámetros de red, la durabilidad, niveles, etc). La figura
\ref{fig:arquitecturanodoOSDDS} muestra la arquitectura de un nodo en
\emph{OpenSplice DDS}.


\begin{figure}[center]
  \begin{center}
    \includegraphics[width=\textwidth]{arquitecturanodoOSDDS.png}
    \caption{Arquitectura de un nodo de la tecnología \emph{OpenSplice DDS}}
    \label{fig:arquitecturanodoOSDDS}
  \end{center}
\end{figure}


\subsection{Características de OpenSplice DDS}
\label{sec:caract-de-openspl}
La última versión de \emph{OpenSplice} \acs{DDS} es la versión 6 y se
caracteriza por:
\begin{description}
\item [Múltiples arquitecturas] - Proporciona opciones de desarrollo
  configurables que permiten adaptarse a las características de rendimiento,
  escalabilidad y tolerancia a fallos que envuelven las
  necesidades de un determinado sistema, reduciendo así los costes iniciales y
  los costes durante el desarrollo.
\item[Múltiples paradigmas] - \emph{OpenSplice V6} ofrece una solución adecuada
  al problema de interacción entre distintos modelos de comunicación como son
  los modelos publicador/suscriptor, cachés de objetos distribuidos e
  invocación de métodos remotos (RMI). Además, la calidad de servicio (QoS)
  proporcionado por \acs{DDS} puede ser utilizada para controlar la calidad de
  servicio asociada con servicios individuales, así como con invocaciones
  específicas.
\item[Tiempo real y escalable] - Máxima escalabilidad y mínimas latencias junto
  con eventos en tiempo real ofrecen sistemas de tiempo real a gran
  escala. Para ello, \emph{OpenSplice DDS} proporciona dos características
  principales: un protocolo de red de tiempo real optimizado y escalabilidad;
  y opciones de despliegue de memoria compartida para minimizar el uso de los
  recursos, optimizando la comunicación entre nucleos y aplicando las políticas
  de tráfico de red a los nodos del sistema.
\item[Conectividad] - La puerta de enlace \emph{OpenSplice} proporciona soporte
  para unas 80 conexiones a otras tecnologías de mensajería (por ej.
  JMS\footnote{Java Message Service, es un \acs{API} que se utiliza para el uso
    de colas de mensajes. Permite crear, enviar, recibir y leer mensajes},
  AMQP\footnote{Advanced Message Queuing Protocol es un protocolo utilizado en
    la capa de aplicaciones de un sistema de comunicación. Soporta orientación a
  mensajes, uso de colas y enrutamiento (punto-a-punto y
  publicación-suscripción)}) y a tecnologías Web propietarias (por ej. W3C Web
Services) (figura \ref{fig:gatewayOpenSplice}).
\item[Herramienta de pruebas] - \emph{OpenSplice DDS v6} proporciona una
  herramienta para realizar pruebas que simplifican el testeo de los sistemas
  distribuidos basados en \acs{DDS}.
\item[Código abierto] - \emph{OpenSplice v6} está disponible bajo la licencia
  \emph{LGPLv3} y también bajo la propia licencia de \emph{PrismTech
    Commercial}.
\item[Cumplimiento de estándares] - \emph{OpenSplice DDS} es una estricta
  implementación del estándar \acs{DDS} de \acs{OMG}. Esto garantiza la
  portabilidad y la interoperabilidad entre las implementaciones de \acs{DDS}.
\end{description}

\begin{figure}[center]
  \begin{center}
    \includegraphics[width=0.3\textwidth]{gatewayOpenSplice.png}
    \caption{Puerta de enlace de la tecnología \emph{OpenSplice DDS}}
    \label{fig:gatewayOpenSplice}
  \end{center}
\end{figure}


\subsection{Estudio de la comunicación de eventos OpenSplice
DDS}

Se han realizado algunos ejemplos para conocer la dinámica de esta
arquitectura. En este capítulo se muestra uno de estos ejemplos para la
compresión de la dinámica de trabajo de \emph{OpenSplice DDS}.
El \acs{IDL} utilizado para el ejemplo se muestra en el listado
\ref{code:idlHello}.

\begin{listing}[
  language = Slice,
  caption  = {\acs{IDL} utilizado para el ejemplo \emph{HelloWorld} de
    \emph{OpenSplice DDS}},
  label    = code:idlHello]
module HelloWorldData {
  struct Msg
  {
    long userID;
    string message;
  };
  #pragma keylist Msg userID
};
\end{listing}

La implementación de un simple ejemplo es muy sencilla y se puede interpretar
fácilmente. En el listado \ref{code:suscriptorOSDDS} y en el listado
\ref{code:publicadorOSDDS} se pueden ver las correspondientes implementaciones
de un suscriptor y de un publicador para el modelo de comunicaciones
\emph{OpenSpliceDDS}.

En las dos implementaciones se sigue el mismo procedimiento:
\begin{enumerate}
\item Se crea un dominio que va a contener el canal de comunicación entre
  publicador y suscriptor.
\item Se crea el tipo del canal. El tipo de canal viene definido en el
  \acs{IDL} especificado para este ejemplo (listado \ref{code:idlHello}).
\item Una vez obtenido el tipo del canal, se crea el canal correspondiente cuyo
  nombre tiene que ser el mismo tanto en la parte del suscriptor como en la del
  publicador.
\item En cada caso, se crea el publicador del canal y se crea el subscriptor
  del canal.
\item Para el publicador se crea el objeto \texttt{DataWriter} correspondiente
  y para el suscriptor se crea el objeto \texttt{DataReader}.
\item Se envían eventos en el caso del publicador y el subscriptor espera la
  recepción de eventos.
\item Finalmente, se eliminan los \datar{} y \dataw{}, el canal, el suscriptor
  y el publicador.
\end{enumerate}


\lstinputlisting[
language=C++,
caption={Suscriptor de \emph{OpenSplice DDS}},
label=code:suscriptorOSDDS]
{listings/HelloWorldDataSubscriber.cpp}

\lstinputlisting[
language=C++,
caption={Publicador de \emph{OpenSplice DDS}},
label=code:publicadorOSDDS]
{listings/HelloWorldDataPublisher.cpp}

Para probar que, efectivamente, los eventos son enviados por el publicador y
recibidos por el suscriptor, se crea una prueba de integración utilizando la
herramienta \texttt{atheist}~\cite{atheist}, cuya implementación se puede
encontrar en el directorio \texttt{test/samples/}.

\subsection{Estudio del filtrado de eventos}
\label{sec:estudio-del-filtrado}

Se realiza un estudio de la comunicación entre un subscriptor y un publicador
en la tecnología \emph{OpenSplice DDS} utilizando para ello un canal cuyo tipo
será una estructura semejante a los eventos utilizados en el proyecto
\emph{Elcano}. El listado \ref{code:eventotipoelcano} muestra el \acs{IDL}
definido para ello.

\begin{listing}[
  language = Slice,
  caption  = {\acs{IDL} que especifica el tipo de canal con una estructura de
    datos semejante a la utilizada en los eventos del proyecto \emph{Elcano}},
  label    = code:eventotipoelcano]
module EventElcanoFilter
{
  struct Point {
    double x;
    double y;
    double z;
  };

  struct LocEvent
  {
    string provider;
    string msid;
    long time;
    Point point;
  };
  #pragma keylist LocEvent provider
};
\end{listing}

En este ejemplo se analiza el tráfico de la red cuando hay filtrado de
eventos. La dinámica del subscriptor y del publicador son muy similares a las
implementadas en el ejemplo \emph{HelloWorld} antes mencionado. Las nuevas
operaciones se pueden ver en el listado \ref{code:crearcanalfiltradoOSDDS}
donde el suscriptor crea un canal filtrado (\texttt{ContentFilteredTopic})
indicando el filtro que se realizará en los eventos recibidos.

\begin{listing}[
  language = C++,
  caption  = {Operaciones realizadas para la creación de un canal filtrado en
    \emph{OpenSplice DDS}},
  label    = code:crearcanalfiltradoOSDDS]
  char sTopicName[] = "MyLocEventTopic";

  // create subscription filter
  ostringstream buf;
  buf << "provider = '" << filter << "'";
  CORBA::String_var sFilter = CORBA::string_dup(buf.str().c_str());

  // Filter expression
  StringSeq sSeqExpr;
  sSeqExpr.length(0);

  // create topic
  mgr->createContentFilteredTopic(sTopicName, sFilter.in(), sSeqExpr);

  // create Filtered DataReader
  cout << "=== [EventElcanoFilterSubscriber] Subscription filter : "
       << sFilter << endl;
  mgr->createReader(true);

\end{listing}

\subsubsection{Análisis del tráfico en la herramienta
\emph{Wireshark}}
\label{sec:analisis-del-trafico}

La herramienta \emph{Wireshark}~\cite{wireshark} ofrece un análisis del tráfico
que circula por la red (información de cada paquete, protocolos, \...). El
interés de utilizar esta herramienta radica en conocer qué información es
enviada en los paquetes que contiene los eventos que se envían desde los
publicadores a los suscriptores.

Se ha analizado el tráfico entre un publicador y un suscriptor ejecutados en
diferentes máquinas. El publicador tiene la ip 161.67.106.124 y el suscriptor
tiene la ip 161.67.106.141.

El suscriptor se registra al canal con el filtro \emph{WIFI}, es decir,
recibirá todos los eventos cuyo proveedor sea \emph{WIFI}.

En la figura \ref{fig:paqSubscription} se puede observar cómo en la máquina que
ejecuta el suscriptor se registra indicando el nombre del
canal(\emph{LocEventTrakerExclusive}), el tipo de dato al que se
suscribe(\emph{LocEvent}) y se indica también el nombre de dominio del
participante(\emph{EventElcanoFilterTopic example}). Del mismo modo, en la otra
máquina se crea el publicador.

\begin{figure}[center]
  \begin{center}
    \includegraphics[width=0.8\textwidth]{paqSub.png}
    \caption{El consumidor se suscribe al canal}
    \label{fig:paqSubscription}
  \end{center}
\end{figure}

A partir de la creación del publicador, se empiezan a mandar los eventos de
localización. La figura \ref{fig:paqData} muestra el contenido de un paquete
que contiene un evento de localización.

\begin{figure}[center]
  \begin{center}
    \includegraphics[width=0.8\textwidth]{paqdata.png}
    \caption{Paquete que contiene los datos de un evento de localización}
    \label{fig:paqData}
  \end{center}
\end{figure}




%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
