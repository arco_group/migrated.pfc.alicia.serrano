% -*- coding: utf-8 -*-

\chapter{Método de trabajo y herramientas}
\label{chap:metodology}

En este capítulo se describe la metodología de desarrollo elegida y las
herramientas utilizadas en la elaboración del proyecto junto con una breve
descripción de cada una de ellas.

\section{Metodología de trabajo}
\label{sec:metodologia}

Desde un principio, el proyecto fue pensado para ser construido sobre un
sistema distribuido. Esto permite la utilización de los diferentes componentes
independientemente de la arquitectura del sistema que lo contenga.

Los requisitos del sistemas son especificados desde el inicio y la interacción
con el cliente es una parte importante del desarrollo. Esta interacción
continua con el cliente permite obtener más información detallada para el
correcta realización del proyecto. Además, en cada iteración proporcionará un
prototipo totalmente funcional que ayudará a perfilar y añadir los requisitos
necesarios para mejorar el producto final.

Por estas razones, la metodología que se ha utilizado es \emph{prototipado
  incremental}.

\subsection{Prototipado incremental}
\label{sec:prototipado_evolutivo}
El \textbf{prototipado incremental}~\cite{ingenieria_software} se basa en la
realización de prototipos funcionales a lo largo del desarrollo del sistema
hasta llegar a un producto final. Estos prototipos son entregados al cliente,
de modo que éste puede comprobar el cumplimiento de los requisitos
especificados y mejorar y/o añadir nuevos. El cliente también puede detectar
errores o carencias que sirvan para refinar con más detalle los requisitos
especificados y contribuir de este modo a una mejora progresiva del prototipo
final en cada iteración.

El esquema general de esta metodología se muestra en
la figura \ref{fig:prototipado_incremental}. En cada iteración, se definen
nuevas funcionalidades del sistema, se describen más detalladamente y se
desarrollan. El prototipo obtenido se integra en el sistema y se entrega al
cliente.

\begin{figure}[!h]
  \begin{center}
    \includegraphics[width=0.8\textwidth]{prototipado_incremental.jpg}
    \caption{Diagrama de flujo del prototipado incremental}
    \label{fig:prototipado_incremental}
  \end{center}
\end{figure}

Las ventajas de utilizar esta metodología son las siguientes:
\begin{itemize}
\item Los clientes puede utilizar el sistema desde las primeras fases de
  desarrollo, ya que se obtienen prototipos intermedios con los que poder
  trabajar. Esto proporciona al cliente mayor confianza y creencia en la
  obtención final de un producto que cumplirá con los requisitos
  especificados.
\item El desarrollo de prototipos intermedios hace más fácil determinar si los
  nuevos requisitos planteados en las siguientes iteraciones son correctos y
  viables.
\item El riesgo de errores es muy bajo y los fallos pueden ser fácilmente
  solventados. En cada iteración se prueba el prototipo realizado y además se
  realizan las pruebas de las iteraciones anteriores para asegurar que el
  prototipo cumple también con las funcionalidades añadidas anteriormente.
\item Las funcionalidades más importantes se entregan en las primeras fases de
  desarrollo y es, por lo tanto, la parte del sistema que se somete a más
  pruebas. Esto conlleva al desarrollo de un sistema más fiable.
\end{itemize}

Aunque no todo son ventajas. Disponer de prototipos intermedios puede conducir
a situaciones poco convenientes:
\begin{itemize}
\item El cliente participa activamente en el proceso de desarrollo, lo que hace
  que los requisitos puedan ser modificados continuamente debido a los cambios
  que el cliente crea oportunos.
\item El mantenimiento del sistema puede ser problemático debido a la
  especificación de nuevas funcionalidades en su desarrollo. Los
  desarrolladores, por su parte, pueden especializar demasiado el producto lo
  que dificultaría un futuro mantenimiento por el personal que no está
  familiarizado.
\end{itemize}

Para solventar en la medida de lo posible estos problemas, se ha optado por una
combinación de esta metodología con el desarrollo dirigido por pruebas.

\subsection{Desarrollo dirigido por pruebas}
\label{sec:desarrollo_pruebas}

Desde el principio, el planteamiento fue realizar la implementación de este
proyecto usando una metodología ágil, basando esta idea en el previo
conocimiento de las ventajas que esta técnica ofrecía.

\acf{TDD} es una técnica incluida en lo que se conoce como metodología
XP~\cite{xp}. La filosofía de esta técnica es crear las pruebas antes que el
código. Cada prueba garantiza el cumplimiento de un requisito funcional
concreto.

Las ventajas de esta técnica son numerosas y algunas de las más
importantes son:

\begin{itemize}
\item Sólo se implementa lo necesario para satisfacer la prueba y no más. No
  habrá código que no sea necesario.
\item Se disminuye el número de errores. Cualquier modificación que
  afecte a algún componente se detecta y se resuelve fácilmente.
\item El código es reutilizable y se puede cambiar con relativa facilidad.
\item La productividad aumenta considerablemente porque se invierte menos
  tiempo en la depuración.
\end{itemize}

Para poder seguir esta técnica se deben tener claros los escenarios que se
quieren probar. Los escenarios serán la colección de pruebas que servirán para
ir construyendo la \acs{API} del sistema. La descripción de
los escenarios se hace utilizando la técnica \acf{BDD}. Esta técnica utiliza
una plantilla que permite entender mejor el funcionamiento global de sistema:

\begin{itemize}
\item \textbf{Given}: Dado un contexto inicial.
\item \textbf{When}: Cuando se produce un evento.
\item \textbf{Then}: Entonces se obtienen unos resultados.
\end{itemize}


\section{Herramientas}
\label{sec:herramientas}

\subsection{Lenguajes de programación}
\label{sec:lenguajes}

\begin{description}
\item[Python] - lenguaje de alto nivel, interpretado, multiparadigma y
  orientado a objetos, creado por Guido van Roosum. Se utiliza este lenguaje
  debido a la simplicidad del código y su amplia librería estándar.

  Este lenguaje se usa para implementar el particular modelo de
  comunicación de eventos \acs{DDS} y también es usado para la implementación
  de las pruebas realizadas.

\item[C++] - lenguaje que proporciona mecanismos de orientación a
  objetos y es compatible con C. Fue creado por Bjarne Stroustrup.

  C++ se utiliza para implementar los ejemplos básicos de \emph{OpenSplice} y
  \emph{RTI DDS}.

\item[Java] - es un lenguaje de programación a objetos desarrollado por
  Sun Microsystems. Android está fuertemente ligado a java, es por ello que se
  utiliza este lenguaje para la implementación de la aplicación para un
  dispositivo móvil o tablet con Android.
\end{description}

\subsection{Aplicaciones de desarrollo}
\label{sec:aplicaciones}

\begin{description}
\item[ZeroC Ice] - Middleware de comunicaciones orientado a objetos,
  multilenguaje y multiplataforma desarrollado por la empresa ZeroC~\cite{ice}.

  Este middleware es una parte importante del proyecto ya que se utilizan su
  servicio \emph{IceStorm} para la realización de un modelo de comunicación
  atendiendo al estándar \acs{DDS}.

\item[GNU Make] - herramienta para la generación automática de
  ejecutables~\cite{make}.

\item[\acf{GCC}] - colección de compiladores del proyecto
  GNU~\cite{gcc}.

\item[GNU Emacs] - entorno de desarrollo de GNU que se ha utilizado tanto para
  la implementación como para la documentación~\cite{emacs}.

\item[Eclipse] - entorno de programación~\cite{eclipse} utilizado para el
  desarrollo de la aplicación móvil con sistema operativo \emph{Android}. La
  integración del \emph{SDK de Android} en este entorno de programación es muy
  sencilla y fácil de manejar~\cite{android}.

\item[Mercurial] - sistema de control de versiones distribuido. Se ha hecho uso
  de esta herramienta para el código del proyecto y la
  documentación~\cite{mercurial}.

\item[Atheist] - plataforma de pruebas con la que se elaboran las pruebas de
  integración de este proyecto. Es un herramienta desarrollada por el grupo
  \acs{Arco}~\cite{atheist}.

\end{description}

\subsection{Documentación}
\label{sec:documentacion}

\begin{description}
\item[\LaTeX] - lenguaje de marcado de documentos de carácter técnico y
  científica, utilizado para realizar este documento~\cite{latex}.

\item[BibTex] - herramienta para la descripción de referencias para documentos
  escritos con \LaTeX.

\end{description}

\subsection{Gestión de proyecto}
\label{ssec:management}

\begin{description}
\item[Redmine] - Aplicación web para la gestión de proyectos. Mediante esta
  herramienta se han establecido las tareas a realizar a lo largo del ciclo de
  vida del proyecto. Utilizando esta herramienta se tiene un visión constante
  del desarrollo del proyecto (tiempo dedicado a cada tarea, tareas por
  terminar, nuevas tareas, etc.)~\cite{redmine}.

\end{description}

\subsection{Herramientas hardware}
\label{sec:herr-hardw}

\begin{description}
\item[Motorola Tablet PC] - Tablet con sistema operativo Android 4.0.
\end{description}


%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
