
\chapter{Desarrollo del proyecto}
\label{chap:desarrollo}

Este capítulo describe la planificación y desarrollo del proyecto. Se definen
los requisitos y las distintas iteraciones que se llevan a cabo, así como los
prototipos obtenidos en cada una de ellas. Además, la implementación se
describe en base a las pruebas que se realizan para validar el cumplimiento de
los requisitos iniciales.

\section{Especificación de requisitos}
\label{sec:requisitos}

Tras el estudio del estándar \acs{DDS} de la \acs{OMG} se ha adquirido un
conocimiento más detallado de la funcionalidad que debe tener un modelo de
comunicaciones basado en este estándar. Como se menciona en los objetivos de la
sección \ref{sec:objetivos:secundarios}, este proyecto se centra en los
aspectos relativos al filtrado de eventos. La figura \ref{fig:overview} muestra
la parte de la especificación del estándar \acs{DDS} que se persigue y que
servirá como guía para el desarrollo del sistema.

\begin{figure}[center]

  \begin{center}
    \includegraphics[width=1.0\textwidth]{Overview.png}
    \caption{Visión general para el desarrollo de la API con \emph{ZeroC Ice}}
    \label{fig:overview}
  \end{center}
\end{figure}

A partir del conocimiento adquirido debido al estudio del estándar \acs{DDS} y
de las implementaciones \emph{RTI DDS} y \emph{OpenSplice} (apéndice
\ref{cha:casos-de-estudio}, y atendiendo a las diferentes funcionalidades que
aporta el servicio \emph{IceStorm} de \emph{ZeroC Ice}, se definen los
siguientes requisitos funcionales:

\begin{itemize}
\item Se utilizará el servicio \emph{IceStorm} que añadirá la funcionalidad
  necesaria para la gestión y administración de los canales \acs{DDS}.
\item Se debe disponer de un gestor de canales \acs{DDS}. Este componente se
  encargará de crear los canales necesarios atendiendo a los parámetros
  solicitados.
\item Los canales podrán ser de dos tipos: canales generales donde tienen
  cabida eventos de todo tipo y canales donde se indican filtros. Estos últimos
  canales limitan la comunicación de ciertos eventos atendiendo a la
  descripción de los filtros indicados.
\item Un canal podrá tener múltiples suscriptores y publicadores.
\item Un publicador será el encargado de enviar eventos al canal del que es
  publicador.
\item Un suscriptor se podrá registrar en un canal para poder recibir los
  eventos del mismo.
\item Un suscriptor podrá registrarse en uno o más canales a la vez.
\item La suscripción permitirá indicar uno o más filtros. De este modo, los
  suscriptores recibirán únicamente la información que les interese.
\item Un canal puede disponer de publicadores filtrados, es decir, publicadores
  que solo envíen datos que coincidan con los filtros especificados en la
  creación del publicador.
\item Un suscriptor puede anular la suscripción a un canal.
\item El gestor de canales \acs{DDS} tendrá la posibilidad de eliminar
  canales.
\item La federación de \emph{IceStorm} permitirá poder realizar enlaces
  entre canales filtrados. De este modo se reducirá el número de canales
  creados ya que los enlaces permiten la propagación de eventos de unos canales
  a otros.
\end{itemize}


\section{Casos de uso}
\label{sec:casos-de-uso}
El análisis de requisitos permite identificar los diferentes casos de uso que
tendrá el sistema a desarrollar. La figura \ref{fig:casosdeuso} muestra el
diagrama de casos de uso indicando la interacción entre las distintas entidades
que componen el sistema.

\begin{figure}[center]
  \begin{center}
    \includegraphics[width=\textwidth]{casosdeuso.png}
    \caption{Diagrama de Casos de Uso}
    \label{fig:casosdeuso}
  \end{center}
\end{figure}


Cada caso de uso tiene una funcionalidad interna que se detallará más adelante
en cada una de las iteraciones realizadas. Nótese que tanto un suscriptor como
un publicador pueden crear canales sin necesidad de que el sistema tenga una
entidad que se encargue de ello. A modo de resumen, a continuación se muestra
un breve descripción de cada uno de los casos de uso:

\begin{definitionlist}
\item[Crear canal]
El gestor de canales del sistema crea un canal \acs{DDS} propio indicando el
nombre del mismo. Este parámetro representará al canal unívocamente en el
sistema.
\item[Crear canal filtrado]
El gestor de canales del sistema crea un canal filtrado teniendo en cuenta los
parámetros de filtrado y el tipo de datos que tiene que filtrar.
\item[Suscribirse a un canal]
Cierta entidad puede suscribirse a un canal creado para que le sean enviados
los eventos que se publican en él.
\item[Suscribirse a un canal indicando filtros]
Una determinada entidad quiere suscribirse a un canal pero sólo desea obtener
cierta información que se maneja.
\item[Obtener publicador de un canal]
Se adquiere el publicador a un canal creado.
\item[Obtener publicador de canal indicando filtros]
Se obtiene un publicador a un canal creado considerando parámetros de
restricción.
\item[Publicar en un canal]
El publicador envía eventos al canal.
\item[Eliminar suscripción a un canal]
Un suscriptor desea no estar suscrito a un determinado canal.
\item[Destruir un canal]
Un canal puede ser eliminado del sistema.
\item[Listar canales en el sistema]
Se obtiene la lista de canales creados en el sistema.
\item[Enlace a un canal]
Un canal solicita que los eventos que se publican en un determinado canal sean
propagados a él.
\end{definitionlist}

\section{Diseño}
\label{sec:diseno}

Una vez analizados los requisitos del proyecto, se continúa con la fase de
diseño. En esta fase se compone la estructura básica del sistema así como los
componentes que toman parte en ella.

El componente básico del que no se puede prescindir es de un gestor de los
canales de eventos \acs{DDS}. Este componente será el encargado de crear
canales acorde a las solicitudes demandadas. También se encargará de realizar
tareas como listar los canales que existen en el dominio del sistema,
proporcionar un determinado canal y tendrá la posibilidad de destruir uno o más
canales que existen en el dominio del sistema.

El gestor de canales de eventos \acs{DDS} actuará como intermediario entre los
publicadores y suscriptores de los canales del sistema y para ello hará uso del
servicio \emph{IceStorm} de \emph{ZeroC Ice}. El servicio \emph{IceStorm}
proporcionará las ventajas que lo caracterizan como son llamada única para
distribuir la información a los suscriptores, independencia entre suscriptores
y publicadores, creación de canales de manera dinámica, \...

Otro componente será el canal de eventos \acs{DDS}. La funcionalidad propia de
esta canal será proporcionar un mecanismo de suscripción, tanto si la
suscripción se realiza indicando parámetros de filtrado como si se realiza una
suscripción sin indicar el filtrado, que será similar a la suscripción
realizada en los canales \emph{IceStorm}. Además, proporcionará las operaciones
necesarias para obtener el publicador al canal. Al igual que ocurre en la
suscripción, el publicador puede asociarse con unos determinados filtros que
implicará la publicación de los eventos que se ajusten a los mismos.

Por último, el sistema contará con el objeto \emph{Publisher}. Como su propio
nombre indica será el publicador de eventos de un canal. En el momento de la
publicación se encargará de realizar las comprobaciones necesarias de filtrado
para propagar los eventos a los suscriptores que corresponda.

Todos los componentes anteriores constituirán un modelo de comunicaciones
suscriptor/publicador con la característica especial de poder
realizar filtrado en la información que circula por el sistema. La figura
\ref{fig:arquit_icedds} muestra una visión general de la arquitectura que
compondrá el modelo de comunicaciones que se persigue en el desarrollo de este
proyecto.

\begin{figure}[center]
  \begin{center}
    \includegraphics[width=0.8\textwidth]{pub_sub_dds.png}
    \caption{Arquitectura del modelo de comunicaciones suscriptor/publicador
      \emph{IceDDS}}
    \label{fig:arquit_icedds}
  \end{center}
\end{figure}

\section{Implementación}
\label{sec:implementacion}

El desarrollo del proyecto está dividido en diferentes iteraciones. En cada una
de estas iteraciones se irán añadiendo nuevas funcionalidades que aportarán las
operaciones necesarias para ir construyendo el producto final. El modelo de
comunicaciones que se desarrolla en este proyecto se llamará \emph{IceDDS}.

La construcción de \emph{IceDDS} se realiza utilizando el lenguaje de
programación \emph{Python}, debido a la sencillez y claridad de programación
que ofrece.

Hay que destacar que en cada iteración se lleva a cabo un plan de pruebas, tal
y como se explicó en la sección \ref{sec:prototipado_evolutivo}. El plan de
pruebas de desarrollo del proyecto se puede ver en el apéndice \ref{chap:plan}.

Para conocer la dinámica de las pruebas realizadas para cada iteración, a
continuación se muestran ejemplos de los dos tipos de pruebas que se realizan
a lo largo del ciclo del proyecto.

\begin{definitionlist}
\item [Pruebas unitarias]
Como se explica en el capítulo \ref{sec:desarrollo_pruebas}, las pruebas
unitarias siguen la técnica \acs{BDD}. En el listado \ref{code:testunitaria} se
muestra un ejemplo sencillo de prueba unitaria realizada para comprobar que el
subscriptor recibe el evento de un canal.

\begin{listing}[
  language = python,
  caption  = {Ejemplo de prueba unitaria},
  label    = code:testunitaria]
def test_filteredInTopicOneSubscriber(self):
   # given a filtered topic, a publisher and a subcriptor
   topic = self.topicManager.createTopic("foo")
   publisher = Event.ListenerPrx.uncheckedCast(topic.getPublisher())
   subscriber = mocks.Subscriber(self.adapter)
   topic.subscribeAndGetPublisher(subscriber.proxy)

   # when the publisher sends several events
   publisher.sendData(MLP.Coord(1.0, 12.0, 0.0))
   subscriber.servant.wait(2)

   # then the subscriber receives the events
   params = 'send_data', (1.0, 12.0)
   self.assert_(subscriber.servant.method_was_called(*params))

   self.assertEqual(1,
                    len(subscriber.servant.get_registered_calls()))
\end{listing}

En esta prueba se cuenta con un canal (\texttt{topic}), un publicador del
canal (\texttt{publisher}) y un suscriptor (\texttt{subscriber}). Como se puede
observar, el suscriptor es un objeto que imita el comportamiento del sirviente
real de nuestro sistema, es lo que se denomina \emph{mock}. Este objeto
simulará a un suscriptor real y dará el resultado esperado. El sirviente cuenta
con una lista de objetos que guarda los eventos recibidos. De esta manera, se
puede comprobar los datos recibidos en cada prueba realizada.

Para comprobar el correcto funcionamiento del envío de un evento, lo primero
que hay que hacer es suscribir el suscriptor al canal. En este momento, ya se
tienen todos los componentes necesarios para realizar un envío de un dato.

Las comprobaciones que se realizan después de que el publicador envíe un dato
son que la lista contenga solamente el número de datos enviados (en este caso
1) y se comprueba que el dato recibido es el correcto.

La dinámica para el resto de pruebas realizadas es la misma: se crean los
componentes que forman parte de la prueba, se realiza la acción deseada y se
comprueba la recepción de los eventos a los correspondientes suscriptores.

\item[Prueba de sistema]
Las pruebas de sistema se realizan con la herramienta
\texttt{atheist}~\cite{atheist} que proporciona una plataforma para realizar
pruebas de integración del sistema. Las pruebas se basan en términos
declarativos. En el listado \ref{code:testintegracion} se muestra un ejemplo de
prueba de integración en la cual se comprueba que los publicadores han enviado
los eventos y los suscriptores los han recibido.

\lstinputlisting[
  language=python,
  caption={Ejemplo de prueba de integración},
  label=code:testintegracion]
{listings/integracion.test}

Se cuenta con la implementación de un gestor de canales (\texttt{TopicManager})
que se encarga de crear un canal determinado. También se cuenta con las
implementaciones de un publicador y un suscriptor. En la prueba se especifican
pre-condiciones para la ejecución de los elementos anteriores y mediante las
post-condiciones se comprueba el correcto funcionamiento de cada parte. En este
caso se comprobaría que en el suscriptor se han recibido los eventos (2, 3, 4)
y (5, 14, 20).

\end{definitionlist}

A continuación se describen detalladamente cada una de las iteraciones.

\subsection{Suscripción y publicación sin filtros}
\label{sec:crearcanal}

\subsubsection{Análisis y diseño}
El modelo de comunicaciones que se implementará debe tener un gestor de
canales. Este gestor es el encargado de crear los diferentes canales que sirven
como medio de comunicación para los publicadores y suscriptores. Para ello se
necesitará un adaptador de objetos y la configuración necesaria para crear el
gestor de canales \acs{DDS}.

Del mismo modo, se necesita describir las operaciones necesarias para la
suscripción y la publicación en un canal.

\subsubsection{Implementación}
En primera instancia, se lleva a cabo la implementación de las pruebas
unitarias necesarias utilizando la herramienta \emph{nose}~\cite{nose}. En base
a ellas, se implementan las clases \emph{TopicManager} que representa el
servidor de canales de eventos \acs{DDS} y \emph{Topic} que representa a un
determinado canal de eventos \acs{DDS}.

En la creación de un canal, se creará a su vez un canal \emph{IceStorm} que
formará parte de nuestro canal de eventos \acs{DDS}. De
este modo, no se necesita implementar una gestión de canales de eventos ya que
esa funcionalidad la proporciona el servicio \emph{IceStorm}.

En en listado \ref{code:slice} se muestran el slice que contiene las interfaces
definidas hasta el momento.

\begin{listing}[
  language = Slice,
  caption  = {\acs{Slice} para el modelo de comunicaciones DDS con ZeroC Ice},
  label    = code:slice]
module DDS {
   interface Topic
      Object* subscribeAndGetPublisher(Object* subscriber);
      Object* getPublisher();
   };

   interface TopicManager {
      Topic* createTopic(string name);
   };
}
\end{listing}

% La característica principal de \emph{TopicManager} es su parámetro
% \emph{delegate}. Este parámetro es el \emph{TopicManager} de \emph{Icestorm}
% que se utilizará para delegarle el control en las diferentes solicitudes, tanto
% en la creación de canales como en la suscripción y en la publicación de
% eventos.


\subsection{Filtrado de eventos a nivel de canal}
\label{sec:filtradoenelcanal}

\subsubsection{Análisis y diseño}

Se tiene que contar con un procedimiento para crear canales en los que se
limita el tráfico de los datos que no cumplan ciertos requisitos. De este modo
se reduce el tráfico de datos en un canal, pudiendo repartirse entre diferentes
canales.

La implantación de un modelo como este en el proyecto \emph{Elcano} sería una
mejor solución debido a la gran cantidad de información a incluir en los
eventos de localización. Por ello, los datos que se manejarán para la
comunicación entre suscriptores y publicadores serán eventos de localización de
\emph{Elcano}. Estos tipos vienen definidos en
el apartado \ref{sec:event-de-local} del apéndice \ref{chap:slices}. Se
realizará un filtrado teniendo en cuenta las coordenadas del mapa donde se
sitúa el usuario y de este modo poder indicar filtros especificando
determinadas áreas. Por ello, los eventos de localización serán del tipo
\emph{MLP.Coord}.

Un obstáculo que se plantea es el conocer los tipos de datos que son
enviados. Al canal llegarán un conjunto de bytes que deberán ser transformados
en el dato real que envió el publicador. Para que esto se pueda llevar a cabo,
en la creación de un canal con filtro se incluirá una definición del
dato que se va a manejar en el canal.

\subsubsection{Implementación}
Las pruebas realizadas para la comprobación y validación de esta característica
conducen a la necesidad de la creación de los siguientes elementos:

\begin{itemize}

\item \textbf{Filter}

El tipo para los filtros es una secuencia de cadenas (listado
\ref{code:filters}). La decisión de la especificación de los filtros con una
cadena es debida a la posibilidad de indicar gran cantidad de filtros
diferentes sin necesidad de crear objetos específicos.

\begin{listing}[
  language = Slice,
  caption  = {Definición del tipo para filtros},
  label    = code:filters]
  sequence<string> FilterSeq;
\end{listing}

Cada cadena será la expresión del filtro que se compone del nombre de la
variable (x, y ó z) según la coordenada que se quiera filtrar, el operador y
el/los valor/es asociados. Por ejemplo:

\begin{center}
  \texttt{'x in range(1,5)'}
\end{center}



La tabla \ref{tab:filtrosIceDDS} muestra los tipos de operadores que se pueden
especificar para indicar los filtros.

\begin{table}[htbp]
  \centering
  \input{tables/tiposfiltros.tex}
  \caption{Tipos de filtros en IceDDS}
  \label{tab:filtrosIceDDS}
\end{table}

De manera interna, la expresión del filtro es transformada en un objeto
manipulable. En el apéndice \ref{chap:tipos-de-filtros} se describen los
objetos que se crean internamente teniendo en cuenta los filtros especificados.

\item \textbf{TypeCode}

\emph{TypeCode} es el parámetro que indica el tipo de datos del evento. Se
compone de un conjunto de variables y sus tipos asociados. Este parámetro se
necesita porque el canal tiene que saber el contenido de los eventos para poder
extraer los datos y realizar las comparaciones correspondientes con los filtros
especificados.

En el caso de \ac{DDS}, el tipo de canal está compuesto por una estructura
(\texttt{struct}) y para determinar el tipo de un objeto, se hace
introspección de la estructura en tiempo de ejecución.

Agregar introspección de tipos a este proyecto supone un esfuerzo más a incluir
en el desarrollo. Por lo tanto, se plantea la alternativa de indicar el tipo de
datos que manejará el canal con la variable \emph{TypeCode} nombrada
anteriormente, dejando la introspección para un trabajo futuro.

El listado \ref{code:typecode} muestra la definición del parámetro
\emph{TypeCode} que lo constituyen un conjunto de pares de cadenas (nombre de
variable, tipo de variable).

\begin{listing}[
  float=ht,
  language = C,
  caption  = {Tipo de código de los filtros},
  label    = code:typecode]
  struct VariableTypeCode {
    string variableName;
    string variableType;
  };
  sequence<VariableTypeCode> TypeCode;
\end{listing}

\item \textbf{DataDissector}

La clase \emph{DataDissector} se utiliza internamente como un objecto que
contiene la información necesaria de los filtros de un canal.
% El publicador a su vez tiene dos objetos \emph{DataDissector}, ya que tiene
% que tener en cuenta los filtros del canal en el que publica y los propios
% filtros.
Esta clase proporciona un método que comprueba que los valores de los datos
enviados puedan circular por el canal, verificándolos en los filtros
indicados en la creación del canal.

\end{itemize}

Además de los componentes anteriores, la batería de pruebas añade las
siguientes características:
\begin{itemize}
\item Creación de un canal teniendo en cuenta los filtros indicados junto con
  el tipo de datos de los eventos que manejará el canal.
\item Es necesario la realización de la clase \emph{Publisher} ya que será la
  encargada de decidir si un evento puede ser enviado al canal, utilizando para
  ello una comprobación de los filtros del canal en el que se publica. La clase
  \emph{Publisher} hereda de \texttt{Blobject} e implementará el método
  \texttt{ice\_invoke}. Todas las llamadas remotas al sirviente serán
  manejadas a través de la función \texttt{ice\_invoke}. Es en esta función
  donde se hace la comprobación para validar que los valores recibidos
  concuerdan con los filtros especificados, en este caso, en el canal.
\end{itemize}

Para una mayor fiabilidad en los datos enviados, en la creación del canal
filtrado se comprueba que el tipo de código especificado y los filtros
indicados coinciden.

En en listado \ref{code:slice2} se puede ver la especificación del slice con
todos los componentes añadidos en esta iteración.

\begin{listing}[
  language = Slice,
  caption  = {\acs{Slice} para el modelo de comunicaciones DDS con ZeroC Ice},
  label    = code:slice2]
module DDS  {

  struct VariableTypeCode {
    string variableName;
    string variableType;
  };

  sequence<VariableTypeCode> TypeCode;

  sequence<string> FilterSeq;

  interface Topic {
    Object* getPublisher();
    Object* subscribeAndGetPublisher(Object* subscriber);
  };

  interface TopicManager {
    Topic* createTopic(string name);
    Topic* createFilteredTopic(string name, FilterSeq filters,
                               TypeCode eventTypecode);
  };

};
\end{listing}

En la figura \ref{fig:diaseccanalconfiltro} se muestra el diagrama de
secuencia cuyo escenario es el envío de un evento desde un publicador al canal
filtrado. En este escenario se supone que el suscriptor ya está suscrito a
dicho canal.

\begin{figure}[h]
  \begin{center}
    \includegraphics[width=0.9\textwidth]{canalfiltradodiasec.png}
    \caption{Diagrama de secuencia de la creación de un canal con filtros}
    \label{fig:diaseccanalconfiltro}
  \end{center}
\end{figure}


\subsection{Filtrado a nivel de publicador}
\subsubsection{Análisis y diseño}
Además del filtro a nivel de canal, los publicadores que se crean de cada
canal, también tienen que ser capaces de filtrar la información que envían. Los
propios canales pueden tener la intención de filtrar los eventos en
determinadas ocasiones y el sistema, no por ello, tenga que crear otros canales
filtrados para ese propósito. En estas situaciones se crearían publicadores
filtrados que cumplirían las restricciones demandadas por el sistema.

Todo este razonamiento requiere la necesidad de facilitar un mecanismo donde se
pueda establecer un publicador propio de un canal con parámetros de
filtros. Para este tipo de publicadores habrá que indicar, además del filtrado
que se quiere hacer en un determinado publicador, los filtros propios del canal
en que se desea publicar.

Una particularidad que no se ha tenido en cuenta hasta este momento es la
comprobación de que las expresiones que definen los filtros sean proporcionadas
de la manera correcta, es decir, con la estructura que se define en la
iteración anterior: \texttt{``NombreVariable operador/es(tabla
  \ref{tab:filtrosIceDDS}) valor/es''}.

\subsubsection{Implementación}
La colección de pruebas para esta nueva funcionalidad exige la creación de una
nueva operación cuyos parámetros son un nombre que identifique unívocamente al
publicador de un determinado canal en el dominio del sistema, los filtros que
el publicador utilizará para enviar sólo ciertos eventos y el tipo que tienen
los datos que se envían.


El listado \ref{code:slice3} muestra el \acs{Slice} que se define en este punto
del desarrollo del proyecto. Como se puede observar en este listado, se
introduce el lanzamiento de la excepción \texttt{FilterError} mediante la cual
se indica un error en la definición de un cierto filtro y además, en el mensaje
del error se especifica la manera correcta de definir esa expresión.

\begin{listing}[
  language = Slice,
  caption  = {\acs{Slice} para el modelo de comunicaciones DDS con ZeroC Ice},
  label    = code:slice3]
module DDS  {

  struct VariableTypeCode {
    string variableName;
    string variableType;
  };

  sequence<VariableTypeCode> TypeCode;

  sequence<string> FilterSeq;

  exception FilterError {
	string msg;
  };

  interface Topic {
    Object* getPublisher();
    Object* getFilteredPublisher(string publisherName, FilterSeq filters,
                                   TypeCode eventTypecode) throws FilterError;

    Object* subscribeAndGetPublisher(Object* subscriber);
  };

  interface TopicManager {
    Topic* createTopic(string name);
    Topic* createFilteredTopic(string name, FilterSeq filters,
                               TypeCode eventTypecode);
  };

};
\end{listing}

La operación \texttt{getFilteredPublisher} se encarga de crear un publicador
que aplica tanto los filtros del canal al que está asociado, en caso de ser un
canal con filtros, como los propios filtros.

La figura \ref{fig:diasecpubconfiltro} muestra el diagrama de
secuencia cuyo escenario sería la existencia de un publicador filtrado que
manda un evento a canal filtrado.

\begin{figure}[h]
  \begin{center}
    \includegraphics[width=0.9\textwidth]{publicadorfiltradodiasec.png}
    \caption{Diagrama de secuencia para un publicador filtrado}
    \label{fig:diasecpubconfiltro}
  \end{center}
\end{figure}


\subsection{Filtrado a nivel de suscripción}
\label{sec:filtrado-suscripcion}

\subsubsection{Análisis y diseño}

Otra característica interesante que se plantea es que los suscriptores puedan
definir una serie de datos de los que quieren ser informados. Puede ser que en
un canal existan datos en los que el suscriptor no está interesado. Ante esta
situación, el modelo de comunicación del presente proyecto deberá proporcionar
la operación u operaciones necesarias para poder realizar una suscripción a un
canal indicando unos determinados filtros.


\subsubsection{Implementación}

La batería de pruebas implementadas para esta iteración hacen que se necesite
la implementación de una nueva operación para que el suscriptor pueda
especificar que se quiere suscribir a un determinado canal con unos filtros
establecidos. Además, se añade la funcionalidad de poder cancelar una
suscripción de un canal.

Como el formato de los filtros se ha definido en iteraciones anteriores,
solamente hace falta la implementación del propio método que es necesario. De
esta manera se añade al \acs{Slice} del sistema como la función
\texttt{subscribeWithFilters} quedando definido como se muestran en el listado
\ref{code:slice4}.

\begin{listing}[
  language = Slice,
  caption  = {\acs{Slice} para el modelo de comunicaciones DDS con ZeroC Ice},
  label    = code:slice4]
module DDS  {

  struct VariableTypeCode {
    string variableName;
    string variableType;
  };

  sequence<VariableTypeCode> TypeCode;

  sequence<string> FilterSeq;

  exception FilterError {
	string msg;
  };

  interface Topic {
    Object* getPublisher();
    Object* getFilteredPublisher(string publisherName, FilterSeq filters,
                                   TypeCode eventTypecode) throws FilterError;

    Object* subscribeAndGetPublisher(Object* subscriber);
    Object* subscribeWithFilters(Object* subscriber, FilterSeq filters,
                              TypeCode eventTypecode) throws FilterError;
    void unsubscribe(Object* subscriber);

  };

  interface TopicManager {
    Topic* createTopic(string name);
    Topic* createFilteredTopic(string name, FilterSeq filters,
                               TypeCode eventTypecode);
  };

};

\end{listing}


El mecanismo que se sigue en la operación \emph{suscribir con filtros} es el
siguiente:
\begin{enumerate}
\item Se crea un nuevo canal indicando un nuevo nombre y los filtros que se
  indican en la suscripción.
\item Se añade el nuevo canal creado al adaptador de objetos del sistema.
\item El suscriptor se registra en el nuevo canal creado.
\item Se obtiene el publicador del nuevo canal creado.
\item Se enlaza el publicador del nuevo canal. Esto en realidad lo que hace es
  subscribir el publicador al canal principal. De ese modo, se consigue que
  los eventos enviados al canal principal los filtre el publicador del canal
  creado y de ahí propagarlos al suscriptor.
\end{enumerate}


La cancelación de la suscripción a un canal se delega al servicio
\emph{IceStorm} ya que este proporciona la misma funcionalidad.

En la figura \ref{fig:suscripcionconfiltro} y en el diagrama de secuencia
\ref{fig:diasecsubconfiltro} se muestra una visión general de la dinámica de
esta operación. En estos esquemas se puede ver con más claridad la dinámica de
la operación: el suscriptor es registrado en el nuevo canal creado y el
publicador del mismo es el que se encarga de la correspondiente comprobación
para propagar los eventos.

\begin{figure}[h]
  \begin{center}
    \includegraphics[width=0.7\textwidth]{suscripcionconfiltro.png}
    \caption{Procedimiento de una suscripción indicando filtros en el modelo de
      comunicaciones \emph{IceDDS}}
    \label{fig:suscripcionconfiltro}
  \end{center}
\end{figure}

\begin{figure}[h]
  \begin{center}
    \includegraphics[width=0.85\textwidth]{suscriptorfiltradodiasec.png}
    \caption{Diagrama de secuencia de una suscripción con filtros}
    \label{fig:diasecsubconfiltro}
  \end{center}
\end{figure}


\subsection{Federación de canales}

\subsubsection{Análisis y diseño}

Como se explicó en el capítulo \ref{sec:serviciosIce}, la federación permite la
propagación de eventos de un canal a otro. La federación de \emph{IceStorm}
puede restringir la propagación de mensajes asociando un coste a cada
enlace. Cuando un mensaje es publicado en un canal, se compara el coste
asociado de cada uno de sus enlaces con el coste del mensaje y la propagación
se realiza únicamente a los enlaces cuyo coste sea igual o no excede del coste
del mensaje.

En el modelo de comunicaciones del presente proyecto se pretende proporcionar
federación de canales pero teniendo en cuenta una restricción en los datos que
se envían. La propagación de eventos entre canales se hará en función del
filtrado de los datos que se haya especificado en cada enlace.

El uso de la federación de canales permite una reducción del número de
publicadores. Un solo publicador puede servir como publicador de eventos para
varios canales.


\subsubsection{Implementación}

Para implementar esta funcionalidad no se puede utilizar la propia operación
\texttt{link} de \emph{IceStorm} porque solo permite la propagación de todos
los eventos si se indica un coste 0 o, en todo caso, restringir los eventos
según el coste que lleven asociados. Por lo tanto, se implementa una nueva
operación que permite indicar de algún modo un filtrado en los datos que pueden
ser propagados.

La manera de conseguir una propagación acorde a determinados filtros es similar
al mecanismo realizado en la suscripción con filtros. La única diferencia
presente es que los canales implicados existen en el dominio del sistema o son
creados previamente.

La figura \ref{fig:enlacefiltrado} representa el mecanismo de enlace de estas
características. Existen dos canales: canal 1 y canal 2. El canal 2 quiere que
ciertos eventos del canal 1 sean propagados hacia él. Para que esto ocurra, se
crea un publicador del canal 2 que contenga los filtros con los que quiere
realizar el enlace al canal 1. Por último, se hace una llamada a la función
\texttt{linkFiltered} pasándole como parámetro el publicador creado. La propia
función hace una suscripción de este publicador al canal 1. De este modo, el
publicador actúa como un suscriptor filtrado del canal 1 que a su vez filtra
los eventos para propagarlos al canal 2.


\begin{figure}[center]
  \begin{center}
    \includegraphics[width=0.7\textwidth]{enlacefiltrado.png}
    \caption{Federación de canales con filtro en \emph{IceDDS}}
    \label{fig:enlacefiltrado}
  \end{center}
\end{figure}

Al igual que un canal puede enlazarse a otro, también existe la posibilidad de
eliminar ese enlace, para ello, se implementa una funcionalidad cuyo
procedimiento será cancelar el enlace. Realmente, la acción ejecutada
es una anulación de la suscripción del publicador previamente suscrito.

El listado \ref{code:slice5} muestra el \acs{Slice} de \emph{IceDDS} añadiendo
las propiedades anteriores.

\begin{listing}[
  language = Slice,
  caption  = {\acs{Slice} para el modelo de comunicaciones DDS con ZeroC Ice},
  label    = code:slice5]
module DDS  {

  struct VariableTypeCode {
    string variableName;
    string variableType;
  };

  sequence<VariableTypeCode> TypeCode;

  sequence<string> FilterSeq;

  exception FilterError {
	string msg;
  };

  interface Topic {
    Object* getPublisher();
    Object* getFilteredPublisher(string publisherName, FilterSeq filters,
                                   TypeCode eventTypecode) throws FilterError;

    Object* subscribeAndGetPublisher(Object* subscriber);
    Object* subscribeWithFilters(Object* subscriber, FilterSeq filters,
                              TypeCode eventTypecode) throws FilterError;
    void unsubscribe(Object* subscriber);

    void linkFiltered(Object* publisher);
    void unlinkFiltered(Object* publisher);

  };

  interface TopicManager {
    Topic* createTopic(string name);
    Topic* createFilteredTopic(string name, FilterSeq filters,
                               TypeCode eventTypecode);
  };
};

\end{listing}

\subsection{Modificación de filtros de un suscriptor}
\label{sec:modif-de-filtro}

\subsubsection{Análisis y diseño}

Los suscriptores que están interesados en determinados datos pueden cambiar de
criterio según la condiciones en un momento determinado. Por ello, el sistema
debe dar soporte para poder cambiar los criterios de filtrado de un
suscriptor. De esta manera, los eventos que se reciben en
los suscriptores podrán cambiar según las necesidades de estos en determinadas
situaciones.

\subsubsection{Implementación}

Según el mecanismo que se sigue para realizar una suscripción con filtros, un
nuevo canal es creado con los filtros específicos que se indican en la
suscripción. Por medio de este canal es desde donde realmente el suscriptor
recibe los eventos, por lo tanto, es este canal el que debe ser modificado.

Para cambiar el filtro del canal, se añade la operación
\texttt{setFilters}. Este método se encarga de cambiar los filtros
específicos del canal y modificar su publicador para hacer constar que el
filtro para limitar el transporte de datos ha sido cambiado. El canal en el que
realmente está registrado el subscriptor se obtiene al hacer la suscripción, es
decir, el método \texttt{subscribeWithFilters} devuelve el canal creado para,
más tarde, poder modificar sus filtros. Esto conlleva cambiar el tipo del
objeto devuelto en la función \texttt{subscribeWithFilters} por
\texttt{Topic*}, es decir, un objeto tipo canal.

Además de poder cambiar los filtros, también se añade una operación para
conocer los filtros que tiene un canal determinado. Esta operación será
\emph{getFilters}, que devuelve el conjunto de filtros asociado al canal.

En el listado \ref{code:get/setfilters} se pueden ver las dos operaciones que
se añaden en el \acs{Slice} y que son implementadas por la \acs{API}
\emph{IceDDS}.
\begin{listing}[
  language = Slice,
  caption  = {Obtención y modificación de los filtros asociados a un canal},
  label    = code:get/setfilters]
  FilterSeq getFilters();
  void setFilters(FilterSeq filters, TypeCode eventTypecode);
\end{listing}

\subsection{Operaciones del servicio \emph{IceStorm}}
\label{sec:oper-del-serv}


\subsubsection{Análisis y diseño}

El modelo de comunicaciones \emph{IceDDS} debe realizar las mismas operaciones
que ofrece el servicio \emph{IceStorm} además de las descritas
anteriormente. Entonces, se deben añadir las funcionalidades que faltan al
modelo de comunicaciones que se desarrolla en el presente proyecto.

Se usará el servicio \emph{IceStorm} para delegarle todas estas
funcionalidades.

\subsubsection{Implementación}

Se añaden las siguientes operaciones al \acs{Slice}.

\begin{itemize}
\item \textbf{interfaz TopicManager}:
  \begin{itemize}
  \item \texttt{retrieve}
  \item \texttt{retrieveAll}
  \end{itemize}
\item \textbf{interfaz Topic}
  \begin{itemize}
  \item \texttt{getName}
  \item \texttt{link}
  \item \texttt{unlink}
  \item \texttt{destroy}
  \end{itemize}
\end{itemize}

Para implementar estas operaciones no ha hecho falta utilizar la técnica
\acs{TDD} ya que la funcionalidad en cada una de ellas se delega al
\emph{TopicManager} de \emph{IceStorm} y no corresponde en este proyecto probar
el correcto funcionamiento de este servicio. El \acs{Slice} resultante se
muestra en el listado \ref{code:slice6}.


\begin{listing}[
  language = Slice,
  caption  = {\acs{Slice} para el modelo de comunicaciones DDS con ZeroC Ice},
  label    = code:slice6]
module DDS  {

  struct VariableTypeCode {
    string variableName;
    string variableType;
  };

  sequence<VariableTypeCode> TypeCode;

  sequence<string> FilterSeq;

  exception FilterError {
	string msg;
  };

  interface Topic {

    FilterSeq getFilters();
    void setFilters(FilterSeq filters, TypeCode eventTypecode);

    string getName();

    Object* subscribeAndGetPublisher(Object* subscriber);
    Topic* subscribeWithFilters(Object* subscriber, FilterSeq filters,
                              TypeCode eventTypecode) throws FilterError;
    void unsubscribe(Object* subscriber);

    Object* getPublisher();
    Object* getFilteredPublisher(string publisherName, FilterSeq filters,
                                   TypeCode eventTypecode) throws FilterError;

    void link(Topic* linkTo, int cost);
    void unlink(Topic* linkTo);

    void linkFiltered(Object* publisher);
    void unlinkFiltered(Object* publisher);

    void destroy();
  };

  dictionary<string, Topic*> TopicDict;

  interface TopicManager {
    Topic* createTopic(string name);
    Topic* createFilteredTopic(string name, FilterSeq filters,
                               TypeCode eventTypecode);
    Topic* retrieve(string name);
    TopicDict retrieveAll();
  };

};

\end{listing}


\section{Aplicación Android}
\label{sec:aplicacion-android}

Se desarrolla una aplicación Android~\cite{android} para mostrar de manera
visual como funciona el modelo de comunicaciones \emph{IceDDS}. Para el
desarrollo de esta aplicación se ha tomado en cuenta el entorno donde el
proyecto de \emph{Elcano} trabaja.

La aplicación se limita a reproducir los eventos de localización que se pueden
percibir en la planta baja de la \ac{ESI}. Para ello, los usuarios conectados
al sistema deben suscribirse a los distintos canales que existen o a los
canales cuyos datos sean de interés para él.

Además de una suscripción habitual, la aplicación permite suscribirse a los
canales indicando propios filtros. De esta manera, se tienen cubiertas las dos
posibilidades de suscripción que ofrece el modelo de comunicaciones
\emph{IceDDS}.

El cometido de esta aplicación es la visualización de los distintos eventos que
llegan a los canales por medio de los publicadores. Para diferenciar los
eventos de distintos canales y/o las diferentes suscripciones realizadas, la
aplicación representa círculos de diferentes colores. Este tipo de
representación permite ver claramente el filtrado que se realiza, pudiendo
contrastar los diferentes datos que son enviados.

Para una completa guía del uso de la aplicación puede consultarse el apéndice
\ref{chap:manualdeusuario}.






%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
