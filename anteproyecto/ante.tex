\documentclass[a4paper,spanish,11pt]{article}
\usepackage[spanish]{babel}
\usepackage[utf8]{inputenc}

\usepackage{lmodern}
\usepackage[T1]{fontenc}
\usepackage[bookmarks,hyperfootnotes=false,colorlinks=true,urlcolor=blue,linkcolor=black,citecolor=black,pagecolor=black,anchorcolor=black,breaklinks=true]{hyperref}% Hiperenlaces

\usepackage{graphicx}
\graphicspath{{figures/}}

\title{Hola}

%\LARGE Explorer: Herramienta de navegación indoor para personas con necesidades especiales

% Párrafos
\setlength{\parskip}{8pt}

\begin{document}

\begin{titlepage}
\begin{center}
  \includegraphics[width=.3\textwidth]{logos/emblema_informatica-gray.pdf} \\
  \vspace*{0,5cm} {\Large \textsc{Universidad de Castilla-la Mancha\\}}
  \vspace{3mm}
  {\Large \textbf{Escuela Superior de Informática} }  \\
  \vspace{2,5cm}
  {\Large ANTEPROYECTO} \\

  \vspace{1,5cm}{Implementación de una plataforma de propagación de eventos DDS mediante
    objetos distribuidos, y su aplicación a un caso de estudio}

\end{center}
\vspace{2,8cm}
{\large{\textbf{Autor}: Alicia Serrano Sánchez}} \\
{\large{\textbf{Director}:  David Villa Alises}}
\vspace{1,5cm} \\
\begin{flushright}
{\large \today}
\end{flushright}

\end{titlepage}

\newpage
\tableofcontents
\newpage

\section{Introducción}

% Actualmente, las personas discapacitadas tienen el derecho de beneficiarse de
% medidas que garanticen su autonomía, su integración social y profesional y su
% participación en la vida de la comunidad. Este derecho trata de eliminar las
% barreras físicas y también la independencia de las personas con discapacidad en
% el desarrollo de su vida cotidiana así como proporcionarles los medios
% apropiados para realizar tareas de todo tipo.

% La integración de las personas discapacitadas se ve incrementada debido a la
% evolución de la tecnología y de los sistemas de información.

Desde hace unos años, se está utilizando la tecnología como extensión de las
capacidades de las personas, como herramientas capacitantes. Las tecnologías
pueden y deben ayudar a todas las personas, sean cuales sean sus capacidades
funcionales y, especialmente, a aquellas personas con limitaciones. Si las
nuevas tecnologías son diseñadas con carácter inclusivo y de accesibilidad
universal, facilitan y potencian la autonomía.

La tecnología ha aportado respuestas a la discapacidad: teléfonos móviles
adaptados a las necesidades de las personas mayores, ciegos y sordos; PDA que
emplean códigos simbólicos que permiten comunicarse a discapacitados
intelectuales, Internet (con el auge de los servicios de voz); así como los
ordenadores portátiles, las redes inalámbricas, o la domótica para el control
de puertas y entornos en general, tanto fuera como dentro del hogar.

\subsection{Necesidades de comunicaciones}

Actualmente se pueden desarrollar sistemas en los cuales integrar distintas
funcionalidades que proporcionen al usuario las actividades e
información para poder realizar cualquier tarea atendiendo a su
discapacidad. Estos sistemas se componen de varios módulos cada cuál estará
destinado para realizar unas determinadas tareas.

Todo esto necesita de una comunicación entre cada uno de los componentes que
forman el sistema. Para ello, se utiliza un \emph{middleware} que es un
software que permite interactuar o comunicarse con otras aplicaciones
distribuidas y/o software para intercambiar datos entre éstas. Hay múltiples
\emph{middlewares} de comunicaciones que se emplean para sistemas
distribuidos. Ejemplos de estos middleware son las implementaciones de
CORBA\cite{corba}, RMI\cite{rmi}, Web Services\cite{webservices}, Ice\cite{ice}.

Dentro del Grupo de Investigación Arco, dependiente de la Escuela
Superior de Informática de Ciudad Real, se está desarrollando el proyecto
\emph{Elcano}. Este proyecto pretende ayudar a personas con necesidades
especiales a moverse dentro de grandes edificios o recintos públicos. Para ello
se proporciona al visitante un dispositivo móvil interactivo que le ofrecerá la
información adecuada para poder llegar al destino especificado. \emph{Elcano}
localiza al visitante en el entorno utilizando múltiples sistemas de
posicionamiento simultáneamente (WiFi\cite{wifi}, Bluetooth\cite{bluetooth},
RFID\cite{rfid}, marcas visuales en el entorno, ...). Combinando los datos
obtenidos de esos sistemas de posicionamiento heterogéneos se puede lograr
información más precisa.

Para establecer las comunicaciones y obtener la información de interés existen
varios modelos. Una de las maneras para establecer una comunicación es la
utilización del paso de mensajes. Se emplea en programación concurrente y
aporta sincronización de procesos y permite la exclusión mutua. Otro de los
modelos de comunicaciones que se conoce es RMI (Remote Method Invocation). Este
modelo se basa en la arquitectura cliente/servidor y permite a un objeto que se
está ejecutando en una máquina virtual llamar
a métodos de otro objeto que se está ejecutando en otra máquina virtual
diferente. El modelo de comunicaciones que se va a tomar como base en este
proyecto es el modelo publicador/suscriptor. Este modelo permite el
intercambio de información en forma de eventos. El publicador notificará de los
eventos a cada uno de sus suscriptores.

En estos momentos, en el proyecto \emph{Elcano}, se está utilizando el
middleware Ice de ZeroC. Se trata de un middleware que proporciona soporte y
servicios avanzados para el desarrollo de aplicaciones distribuidas basadas en
invocación a método remoto (RMI). Aunque es un enfoque adecuado y sencillo, RMI
no es la mejor solución para el caso de \emph{Elcano}. La enorme variedad de
información a incluir en los eventos de localización y la escalabilidad del
sistema hacen recomendable el uso de middlewares más escalables como Data
Distribution Service (DDS)\cite{omgdds}.

\subsection{Data Distribution Service (DDS)}

DDS es un middleware de tipo publicador/suscriptor que tiene grandes ventajas
prácticas para el transporte de eventos complejos (estructuras con varios
campos), y para el modelado de aplicaciones distribuidas basadas en ese enfoque.

Las aplicaciones se distribuyen en nodos que se comunican con otros nodos
mediante el envío de datos (publicar) o recibiendo datos
anónimamente. Usualmente, un publicador lo único que necesita para comunicarse
con un suscriptor es el nombre y la definición del dato que se va a enviar.

Lo interesante es saber qué datos son comunicados. Una infraestructura
publicador/suscriptor es capaz de entregar los datos a los nodos adecuados sin
necesidad de establecer una comunicación individual. La comunicación está
centrada en los datos y proporciona la habilidad de especificar varios
parámetros. Estos parámetros son los llamados parámetros de Calidad de Servicio
(Quality of Service (QoS)). Los diseñadores del sistema pueden construir de esta
manera una aplicación distribuida basada en requerimientos para cada parte
específica de los datos. De este modo una aplicación se puede suscribir a un
publicador especificando determinados parámetros que harán que sólo reciba los
datos necesarios para realizar ciertas tareas.

\subsection{Proyecto \emph{Elcano}}
El proyecto \emph{Elcano} principalmente se dedica a diseñar y desarrollar una
infraestructura de asistencia a la navegación en grandes entornos públicos
(aeropuertos, bibliotecas, edificios gubernamentales, etc.). Esta
infraestructura permite a personas con alguna discapacidad optimizar y reducir
sus movimientos en dichos espacios para llevar a cabo las tareas asociadas a
los mismos.

Una parte del sistema contiene toda la información que sea necesaria y de
interés para las personas discapacitadas que realizarán alguna tarea. Otra
parte del sistema integra varias tecnologías de posicionamiento de forma que
mediante la aplicación de algoritmos se obtendrá la mayor cantidad de
información posible acerca del usuario. Todo este sistema se muestra en
dispositivos tipo PDA o móviles en el que se representará la información
obtenida del entorno atendiendo a la discapacidad del individuo.

Existen arquitecturas que sirven de base para entender mejor el proyecto
\emph{Elcano}. En en artículo \cite{bcm} se presenta un trabajo llamado
AGAPE (Allocation and Group Aware Pervasive Environment) en el que se describe
una arquitectura para la asistencia en personas mayores en entornos exteriores
e interiores de cara a posibles urgencias. Más centrado en interiores, en
\cite{bbkl} se estudia la mejor forma de representar indicaciones de ruta a
través de un dispositivo portátil atendiendo a las capacidades del mismo y se
centra en la realización de acciones concretas. Otra arquitectura se presenta
en \cite{lsa} donde se investiga la selección de la ruta más adecuada entre
diversas posibilidad de acuerdo al contexto del usuario y sus preferencias.

El problema de las arquitecturas existentes es que se centran en aspectos
concretos, mientras que en el proyecto \emph{Elcano} se aborda el problema de
una forma integral asumiendo todos los pasos necesarios desde el
posicionamiento hasta el guiado de las personas con discapacidad.

\newpage
\section{Objetivos}

El objetivo principal es la implementación libre del estándar \emph{OMG
  DDS}~\cite{omgdds} utilizando el middleware ZeroC Ice\cite{ice}. Esto requiere
un previo entendimiento del funcionamiento de DDS. La finalidad es conseguir
implementar un modelo de comunicaciones que permitirá
compartir datos que serán independientes de la localización geográfica o de la
arquitectura del resto del sistema.

A continuación se mostrará un ejemplo práctico de lo implementado con el
proyecto \emph{Elcano}. Debe existir una actualización continua de los
movimientos y desplazamientos que realiza la persona que interactúa con el
sistema \emph{Elcano}, así como de las tareas que quiera realizar. Por ello, la
aplicación Android que se realizará tiene que poder interactuar con los usuarios
atendiendo a su discapacidad. Se asumen dos tipos de usuario, uno de
ellos con discapacidades físicas que le obliga a ir en silla de ruedas y el
otro con deficiencias en la percepción visual.

\subsection{Objetivos específicos}
\begin{itemize}
\item Estudio de las implementaciones de DDS de RTI\cite{rti},
  OpenSplice\cite{opensplice} y TAO\cite{tao}.
\item Modelado e implementación de eventos DDS utilizando el middleware
  \emph{ZeroC Ice}.
\item Aplicación del modelo de comunicaciones desarrollado a los eventos del
  proyecto \emph{Elcano}.
\item Herramienta de monitorización que permita ver los eventos de comunicación
  entre un publicador y un suscriptor.
\item Representación del funcionamiento del modelo en un dispositivo móvil o
  en tablet cuya implementación será en Android\cite{android}.
\end{itemize}

\newpage
\section{Métodos y fases de trabajo}
Lo primero que se realizará será el estudio del estándar DDS y las
implementaciones existentes más relevantes, tanto libres como comerciales de
DDS, en especial RTI DDS. Con este estudio se tratará de comprender la forma de
publicar y suscribir que propone DDS para luego poder simularla mediante una
interfaz de \emph{ZeroC Ice} específica.

Tras el estudio se pasará a desarrollar la filosofía de comunicación de DDS
sobre la interfaz \emph{ZeroC Ice}. La parte a implementar se centrará en los
perfiles de calidad de servicio relativos al filtrado de eventos en el
\emph{publisher}. Opcionalmente se considera la posibilidad de implementar el
sistema de suscripción \emph{Peer-to-Peer}.

La tercera fase consistirá en realizar una comparativa tanto en aspectos de
eficiencia como en usabilidad entre el sistema desarrollado y las
implementaciones de DDS estudiadas previamente.

Una vez realizada la implementación de la comunicación de DDS hay que adaptarla
a los eventos del proyecto \emph{Elcano}. Los eventos pueden ser de tres tipos:
\begin{itemize}
\item \textbf{Evento de localización:} la aplicación deberá posicionar al
  usuario en el mapa para ser localizado en todo momento.
\item \textbf{Evento de seguimiento:} es una instrucción que aportará la
  aplicación para seguir una ruta (ejemplo, gire a la derecha, siga adelante,
  etc.).
\item \textbf{Evento de ruta:} la aplicación mostrará en el mapa la ruta a
  seguir elegida por el usuario.
\end{itemize}

Por último se realizará una aplicación gráfica sobre un dispositivo móvil con
el sistema operativo Android que tenga las siguientes características:
  \begin{itemize}
  \item El dispositivo móvil se conectará al sistema \emph{Elcano} y se
    obtendrá la información relativa al entorno (mapas y tareas) y se mostrará
    al usuario de forma acorde a su discapacidad (de forma gráfica o mediante
    audio).
  \item Se permitirá al usuario indicar la tarea a realizar mediante voz o
    selección en un menú gráfico.
  \item Una vez que el usuario elija el punto al que quiere dirigirse la
    aplicación representará los eventos que el sistema \emph{Elcano} le
    proporciona (localización, seguimiento y ruta).
  \end{itemize}

\newpage
\section{Medios que se pretenden utilizar}

Las herramientas y entornos de programación que se utilizarán serán Software
Libre.

\subsection{Recursos software}
\begin{itemize}
\item GNU Make: herramienta para la generación automática de
  ejecutables \cite{make}.
\item GNU Compiler Collection: colección de compiladores del proyecto
  GNU \cite{gcc}.
\item Python e iPython: intérpretes del lenguaje de programación
  Python.
\item GNU Debugger (gdb): depurador estándar de GNU \cite{gdb}.
\item GNU Emacs: editor de textos extensible y configurable \cite{emacs}.
\end{itemize}

Los lenguajes que se pretenden utilizar para la realización del
proyecto serán:

\begin{itemize}
\item C++: lenguaje de programación multiparadigma \cite{c++}.
\item Python: Lenguaje de programación orientado a objetos
  interpretado \cite{python}.
\item Java: lenguaje de programación orientado a objetos.
\item Emacs: editor de texto que se utilizará para la documentación y para la
  edición de código.
\item Eclipse: entorno de desarrollo \cite{eclipse}.
\item \LaTeX: lenguaje de marcado de documentos, utilizado para
  realizar este documento y, en un futuro, la memoria del proyecto
  \cite{latex}.
\item BIB\TeX: herramienta para generar las listas de referencias
  tanto de este documento como de la memoria del proyecto.
\end{itemize}

\subsection{Recursos hardware}
\begin{itemize}
\item Dispositivo móvil HTC.
\item Motorola Tablet PC.
\end{itemize}

\newpage
\pagestyle{empty}
\begin{thebibliography}{biblio}
\bibitem{android} Android developers, Open Source Project,
  \url{http://developer.android.com/index.html}
\bibitem{bbkl} Andreas Butz, Jörg Baus, Antonio krüger, and Marco Lohse. A
  hybrid indoor navigation system. In \emph{Proceedings of the 6th
    international conference on Intelligent user interfaces}, IUI'01, pages
  25-32, New York, NY, USA, 2001. ACM.
\bibitem{bluetooth} Bluetooh Technology, Bluetooth SIG Management,
  http://www.bluetooth.com
\bibitem{corba} Common Object Request Broker Architecture, Object Management
  Group, 2008, \url{http://www.corga.org/}
\bibitem{c++} C++ Programming Language, B. Stroustrup, Addison Wesley, 3ª
  Edición, 1999
\bibitem{gdb} Debugging with GDB, The GNU Source-Level Debugger, R.M. Stallman,
  R. Pesch, and S. Shebs, Free Software Foundation, 2002,
  \url{http://ftp.gnu.org/old-gnu/Manuals/gdb-5.1.1/html\_node/gdb\_toc.html}
\bibitem{bcm} D.Bottazzi, A. Corradi, and R. Montanari. Contex-aware
  middleware solutions for any-time and anywhere emergency assitance to elderly
  people. \emph{Communications Magazine}, IEEE, 44(4):82-90, 2006.
\bibitem{lsa} Fernando Lyardet, Diego Wong Szeto, and Erwin
  Aitenbichler. Context-aware indoor navigation. In \emph{Proceedings of the
    European Conference on Ambient Intelligence}, AmI'08, pages 290-307,
  Berlin, Heidelberg, 2008. Springer-Verlag.
\bibitem{emacs} GNU Emacs manual (version 23), Free Software Foundation, 2011,
  \url{http://www.gnu.org/software/emacs/}
\bibitem{make} GNU Make Manual (version 3.82), Free Software Foundation, 28
  Julio 2010, \url{http://www.gnu.org/software/make/manual}
\bibitem{ice} Internet Communications Engine, ZeroC, \url{http://www.zeroc.com}
\bibitem{rmi} Java Remote Method Invocation, Oracle Java Technology,
  \url{http://www.oracle.com/technetwork/java/javase/tech/index-jsp-136424.html}
\bibitem{latex} LaTeX, una imprenta en sus manos; Cascales Salinas, Bernardo
  y Lucas Saorín, Pascual y Mira Ros, José Manuel y Pallarés Ruiz,
  Antonio Sánchez-Pedreño Guillén, Salvador; Aula Documental de Investigación,
  2000.
\bibitem{omgdds} Object Management Group, Data Distribution Service, Octubre
  2006, \url{http://www.omgwiki.org/dds}
\bibitem{opensplice} OpenSplice DDS from Prismtech,
  \url{http://www.prismtech.com/opensplice}
\bibitem{python} Python Reference Manual (version 2.6.7), van Rossum, G., 12
  Junio 2011, \url{http://docs.python.org/release/2.6.7/reference/index.html}
\bibitem{rfid} Radio Frecuency Identification, \url{http://www.rfid.org/}
\bibitem{rti} Real-Time Innovations Data Distribution Service (RTI DDS),
  \url{http://www.rti.com/products/dds/index.html}
\bibitem{tao} TAO's Data Distribution Service, Objetive Interface Systems,
  \url{http://www.omgwiki.org/dds/content/document/tao-data-distribution-service}
\bibitem{eclipse} The Eclipse Foundation open source community
  \url{http://www.eclipse.org}
\bibitem{gcc} Using GCC: The GNU Compiler Collection
  (version 4.7.0), Free Software Foundation, 2010,
  \url{http://gcc.gnu.org/onlinedocs/gcc/}
\bibitem{webservices} Web Services Architecture Working Group,
  \url{http://www.w3.org/TR/ws-gloss/}
\bibitem{wifi} WiFi - Alliance, \url{http://www.wi-fi.org/}













\end{thebibliography}

\end{document}
