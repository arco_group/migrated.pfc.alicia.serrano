package esi.pfc;

import org.mapsforge.android.maps.MapActivity;
import org.mapsforge.android.maps.MapView;

import android.os.Bundle;

public class BasicMapViewer extends MapActivity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		 super.onCreate(savedInstanceState);
		 MapView mapView = new MapView(this);
		 mapView.setClickable(true);
		 mapView.setBuiltInZoomControls(true);
		 mapView.setMapFile("/sdcard/esiplantabaja.map");
		 setContentView(mapView);
	}
}
