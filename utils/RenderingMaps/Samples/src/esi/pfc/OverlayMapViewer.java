package esi.pfc;

import org.mapsforge.android.maps.ArrayCircleOverlay;
import org.mapsforge.android.maps.GeoPoint;
import org.mapsforge.android.maps.MapActivity;
import org.mapsforge.android.maps.MapView;
import org.mapsforge.android.maps.OverlayCircle;

import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;

/**
 * An application which demonstrates how to use different types of overlays.
 */
public class OverlayMapViewer extends MapActivity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		MapView mapView = new MapView(this);
		mapView.setClickable(true);
		mapView.setBuiltInZoomControls(true);
		mapView.setMapFile("/sdcard/esiplantabaja.map");
		setContentView(mapView);

		GeoPoint geoPoint1 = new GeoPoint(0.09, 0.00); 
		GeoPoint geoPoint2 = new GeoPoint(0.00, 0.09); 

		// create the default paint objects for overlay circles
		Paint circleDefaultPaintFill = new Paint(Paint.ANTI_ALIAS_FLAG);
		circleDefaultPaintFill.setStyle(Paint.Style.FILL);
		circleDefaultPaintFill.setColor(Color.BLUE);
		circleDefaultPaintFill.setAlpha(64);

		Paint circleDefaultPaintOutline = new Paint(Paint.ANTI_ALIAS_FLAG);
		circleDefaultPaintOutline.setStyle(Paint.Style.STROKE);
		circleDefaultPaintOutline.setColor(Color.BLUE);
		circleDefaultPaintOutline.setAlpha(128);
		circleDefaultPaintOutline.setStrokeWidth(10);

		// create an individual paint object for an overlay circle
		Paint circlePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		circlePaint.setStyle(Paint.Style.FILL);
		circlePaint.setColor(Color.MAGENTA);
		circlePaint.setAlpha(96);
		circlePaint.setStrokeMiter(2.0f);

		// create the CircleOverlay and add the circles
		ArrayCircleOverlay circleOverlay = new ArrayCircleOverlay(circleDefaultPaintFill,
				circleDefaultPaintOutline, this);
		OverlayCircle circle1 = new OverlayCircle(geoPoint1, 300, null);
		OverlayCircle circle2 = new OverlayCircle(geoPoint2, 100, circlePaint, null, null);
		circleOverlay.addCircle(circle1);
		circleOverlay.addCircle(circle2);


		mapView.getOverlays().add(circleOverlay);

	}
}