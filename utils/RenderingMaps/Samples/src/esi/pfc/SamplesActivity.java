package esi.pfc;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;

public class SamplesActivity extends Activity {

	// Hay que ejecutar el simulador con la orden
	// -sdcard SDKdirectorio/tools/sdcard

    @Override
    protected void onCreate(Bundle savedInstanceState) {
    	super.onCreate(savedInstanceState);

    	Button button1 = new Button(this);
    	button1.setText("BasicMapViewer");
    	button1.setOnClickListener(new OnClickListener() {
    		public void onClick(View view) {
    			startActivity(new Intent(SamplesActivity.this, BasicMapViewer.class));
    		}
    	});
    	
		Button button2 = new Button(this);
		button2.setText("OverlayMapViewer");
		button2.setOnClickListener(new OnClickListener() {
			public void onClick(View view) {
				startActivity(new Intent(SamplesActivity.this, OverlayMapViewer.class));
			}
		});

    	Button button3 = new Button(this);
    	button3.setText("OverlayRouteMapViewer");
    	button3.setOnClickListener(new OnClickListener() {
    		public void onClick(View view) {
    			startActivity(new Intent(SamplesActivity.this, OverlayRouteMapViewer.class));
    		}
    	});
//
//    		Button button3 = new Button(this);
//    		button3.setText("DownloadMapViewer");
//    		button3.setOnClickListener(new OnClickListener() {
//    			public void onClick(View view) {
//    				startActivity(new Intent(Samples.this, DownloadMapViewer.class));
//    			}
//    		});
//


    	setContentView(R.layout.main);
    	LinearLayout linearLayout = (LinearLayout) findViewById(R.id.samples);
    	linearLayout.addView(button1);
    	linearLayout.addView(button2);
    	linearLayout.addView(button3);
//    	linearLayout.addView(button4);
    	
    }
}